# adalm pluto setup

Linux Adalm pluto setup


Install with: 

sudo apt install libiio-dev

Also install:

sudo apt install libiio-utils


Compile with:

gcc -std=gnu99 -g -o plutoCompiled pluto.c -liio -lm -Wall -Wextra


Functions documentation:

https://docs.rs/libiio-sys/0.1.0/libiio_sys/index.html and https://codedocs.xyz/analogdevicesinc/libiio/annotated.html
